---
Spike Report
---

Core Spike 2
============

Introduction
------------

In order to acclimatize to using Blueprints, Unreal Engine's Visual
Programming/Scripting 'language' it would be beneficial to create an
example that demonstrates some basic components and processes used when
working in Blueprints.

Bitbucket Link: <https://bitbucket.org/bSalmon842/2018_spike2>

Goals
-----

1.  In a new Blueprint **FPS** Unreal Engine project, create a
    "Launch-pad" actor, which can be placed to cause the player to leap
    into the air upon stepping on it.

    a.  Play a sound when the character is launched

    b.  Set the Launch Velocity using a variable, which lets you place
        multiple launch-pads with different velocities in the same level

    c.  Add an Arrow Component, and use its rotation to define the
        direction to launch the character

2.  Create a new level using the default elements available to you from
    the Unreal Engine (boxes should suffice), which should be a fun
    little FPS level with many launch-pads that help you access
    different areas.

Personnel
---------

  ------------------------- ------------------
  Primary -- Brock Salmon   Secondary -- N/A
  ------------------------- ------------------

Technologies, Tools, and Resources used
---------------------------------------

-   <https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html>

Tasks undertaken
----------------

1.  Setup an Unreal Default FPS Project

2.  Follow the [Blueprints Quickstart
    Guide](https://docs.unrealengine.com/latest/INT/Engine/Blueprints/QuickStart/index.html)

3.  Attach an Arrow Component and adjust it in the model editor to face
    directly up

4.  Get the Forward Vector of the Arrow and multiply it by the Launch
    Velocity, the player will now be launched in the direction of the
    arrow.

5.  At the end of the blueprint attach a Play Sound at Location node.

6.  Use any sound (The default FPS gunshot noise was used in this
    instance).

7.  Set the Location to 0, 0, 0 so it occurs from the Launchpad.

8.  Create a Level using basic shapes that the player must use the
    Launchpads to traverse

What we found out
-----------------

In this Spike we learned how to create Blueprint Classes in Unreal
Engine 4 as well as how to create a basic Launchpad and expand upon it
which demonstrates methods used for other Blueprint Classes. We also 
learned about basic function of the Unreal Engine, such as use of the
editor to lay out assets in a level, how to create objects and then
attach components to them. In Blueprints we learned how to assign values
to the object from the editor, how to use basic systems such as sound,
and basic Vector math to launch the player using the Arrow Component.
